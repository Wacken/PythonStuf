import unittest
import os, sys
from ImportToAnki import *

excludingTags = ["noun\n","verb\n","pronoun\n","adverb\n","adjective\n","na-adjective\n","suffix\n","expression\n"]

class MakeFileAnkiSuitable(unittest.TestCase):

    def setUp(self):
        self.exampleFileName = "beginner-lessons-s4.txt"
        self.fileToRead = open(self.exampleFileName,"r",encoding = "utf8")
        self.fileContent = self.fileToRead.readlines()
     
    def testReturnTrueIfFileFound(self):
        raised = False
        try:
            open("beginner-lessons-s4.txt","r",encoding = "utf8")
        except:
            raised = True
        self.assertFalse(raised)
    
    def testCompleteFileIsRead(self):
        lateLine = self.fileContent[300]

        self.assertEquals(lateLine, '病室\n')

    def testDeletingTowNounsTag(self):
        editedConten = removeTags(self.fileContent, excludingTags)

        self.assertEqual("俺\n",editedConten[2])
        self.assertEqual('sugar\n',editedConten[53])
    
    def testDeletingPronounTag(self):
        editedConten = removeTags(self.fileContent, excludingTags)

        self.assertEqual(editedConten[6], 'ずっと\n')
    
    def testDeletingAdverbTag(self):
        editedConten = removeTags(self.fileContent, excludingTags)

        self.assertEqual(editedConten[34], '心配\n')
    
    def testDeletingAdjectiveTag(self):
        editedConten = removeTags(self.fileContent, excludingTags)

        self.assertEqual(editedConten[42], '二番目\n')
    
    def testDeletingVerbsTag(self):
        editedConten = removeTags(self.fileContent, excludingTags)

        self.assertEqual(editedConten[148], '大学院\n')

    def testCreateCorrectFileName(self):
        fileName = createCorrectFileName(self.fileToRead.name)   

        self.assertEqual(fileName,"beginner-lessons-s4_anki.txt")

    def testEditContenOfFile(self):
        content = editContentOfFile(self.exampleFileName)

        self.assertEqual(content[2], "俺\t")

    def testCreateNewFileFromEditedContent(self):
        file = createNewFileFromEditedContent(self.exampleFileName)

        finalFile = open(file.name, encoding = "utf8")
        self.assertEqual(finalFile.readline(), "お嬢さん\tyoung lady, daughter\n")
    
    def testFormatLineBreaks(self):
        formatedText = editContentOfFile(self.exampleFileName)

        self.assertEqual(formatedText[0],"お嬢さん\t")
        self.assertEqual(formatedText[2],"俺\t")

    def testPutStringIntoOneFile(self):
        file = createCompleteFile("test")

        allFiles = open(file.name,encoding = "utf8")
        self.assertTrue(len(allFiles.readlines()) >= 5000)

if __name__ == '__main__':
    unittest.main()
