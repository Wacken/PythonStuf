#!/usr/bin/env python3

import shutil, os, glob, sys

def moveAllFilesinDir(srcDir, rootDir, name):
    makeFolder(rootDir) 
    for filePath in glob.glob(srcDir + '\*'):
        # Move each file to destination Directory
        if name in filePath:
            shutil.move(filePath, rootDir)

def moveAllFilesinDirWithName(srcDir ,rootDir, dstDirRev, dstDirLis, name):
    makeFolders([rootDir, dstDirRev, dstDirRev])
    for filePath in glob.glob(srcDir + '\*'):
        # Move each file to destination Directory
        if name in filePath:
            shutil.move(filePath, dstDirRev)
        elif(filePath not in dstDirRev) and (filePath not in dstDirLis):
            shutil.move(filePath, dstDirLis)

def makeFolder(dirName):
    if not os.path.isdir(dirName):
        os.makedirs(dirName)

def makeFolders(dirs):
    for dirName in dirs:
        makeFolder(dirName)

srcDir = os.getcwd()
for subdir, dirs, files in os.walk(srcDir):
    rootDir = os.getcwd()
    reviewDirectory = rootDir + "\\Vocab"
    LessonDirectory = rootDir + "\\Lesson"
    moveAllFilesinDir(os.getcwd(),rootDir,"Upper Intermediate S4")
#moveAllFilesinDirWithName(os.getcwd() ,rootDir, reviewDirectory, LessonDirectory, "review")


