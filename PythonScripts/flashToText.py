from selenium import webdriver
from time import sleep

username = 'sebastianwalchi@outlook.de'
password = 'Ixcurus1.'
urlWebsite = 'http://www.japanesepod101.com'
seasonStrings = ['newbie-lessons-s2','newbie-lessons-s3','study-in-japan-situational-japanese-for-absolute-beginners','speaking-informal-japanese'
    ,'classic-japanese-pod-101-culture-class','particles','jlpt-s2','beginner-lessons-s4','beginner-lessons-s5','beginner-season-6'
    ,'business-japanese-for-beginners-series','traditional-japanese-songs','must-know-japanese-social-media-phrases','getting-started-with-keigo-upper-beginner'
    ,'listen-and-learn-intermediate-japanese-conversations','japanese-for-everyday-life-lower-intermediate','lower-intermediate-lessons-s2'
    ,'lower-intermediate-lessons-s3','lower-intermediate-lessons-s4','lower-intermediate-lessons-s5','lower-intermediate-season-6','onomatopoeia'
    ,'upper-intermediate-lessons','news-and-current-topics-in-japan','advanced-audio-blog-6','advanced-audio-blog-5','advanced-audio-blog-4','audio-blog-s3'
    ,'audio-blog-s2','audio-blog','upper-intermediate-season-5','upper-intermediate-lessons-s4','upper-intermediate-lessons-s3','upper-intermediate-lessons-s2']


def getFlashToTxt(seasonStrings):
    driver = startFirefox()
    goToPodUserSite(driver)
    for seasonString in seasonStrings:
        goToVocabSite(driver, 'https://www.japanesepod101.com/lesson-library/' + seasonString + '/')
        vocabluarysAsString = getVocabsFromHtml(driver)
        createVocabularyTxt(seasonString, vocabluarysAsString)

def startFirefox():
    driver = webdriver.Firefox()
    driver.implicitly_wait(5)
    return driver

def goToPodUserSite(driver):
    driver.get(urlWebsite)
    joinPod(driver)

def joinPod(driver):
    logInButton = driver.find_element_by_css_selector(".r101-sign-in--a__button")
    logInButton.click()
    sleep(1)
    inputElement = driver.find_element_by_name('amember_login')
    inputElement.send_keys(username)
    sleep(1)
    inputElement = driver.find_element_by_name('amember_pass')
    inputElement.send_keys(password)
    sleep(1)
    inputElement.submit()

def goToVocabSite(driver, seasonURL):
    driver.get(seasonURL)
    driver.find_element_by_css_selector(".cl-collection__icon--vocabulary").click()
    driver.find_element_by_css_selector(".listjs-pagination-show-all").click()

def getVocabsFromHtml(driver):
    driver.find_element_by_css_selector("span.romaji:nth-child(4) > span:nth-child(2)").click()
    driver.find_element_by_css_selector("span.kana:nth-child(5) > span:nth-child(2)").click()
    vocabluaryList = driver.find_element_by_css_selector(".listjs-vocabulary").text
    return vocabluaryList

def createVocabularyTxt(seasonString, vocabluaryList):
    vocabFile = open(seasonString + '.txt', "w+",encoding = "utf8")
    vocabFile.write(vocabluaryList)

getFlashToTxt(['audio-blog'])