import unittest 
def add(x,y): 
    return x+y 
 
class SimpleTest(unittest.TestCase): 
    def testadd1(self): 
        self.assertEquals(add(5,5),10) 

class SimpleTest2(unittest.TestCase): 
    def testadd2(self): 
        self.assertEquals(add(4,6),11) 
 
if __name__ == '__main__': 
    unittest.main() 

