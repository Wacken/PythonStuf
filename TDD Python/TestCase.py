from TestResult import *

class TestCase:
    def __init__(self,name):
        self.name = name
    
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def run(self,result):
        result.testStarted()
        self.setUp()
        try:
            exec("self." + self.name + "()")
        except:
            result.testFailed()
        self.tearDown()



